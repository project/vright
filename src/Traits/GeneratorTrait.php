<?php

namespace Drupal\vright\Traits;

use Drupal\user\Entity\User;

/**
 * Trait GeneratorTrait has useful functions that can be reused.
 *
 * @package Drupal\vright\Traits
 */
trait GeneratorTrait {

  /**
   * That is used to generate user name.
   *
   * @return string
   *   Random name.
   */
  public function nameGenerator() {
    $characters = '0123456789';
    $randomName = 'visitor';
    do {
      $number = 3;
      for ($i = 0; $i < $number; $i++) {
        $index = rand(0, strlen($characters) - 1);
        $randomName .= $characters[$index];
      }
    } while (user_load_by_name($randomName));

    return $randomName;
  }

  /**
   * That is used to generate email address.
   *
   * @return string
   *   An address email.
   */
  public function emailGenerator() {
    $userName = $this->nameGenerator();
    $email = $userName . '@test.com';
    $number = 3;
    $characters = '0123456789';
    $randomNumber = '';
    while (user_load_by_mail($email)) {
      for ($i = 0; $i < $number; $i++) {
        $index = rand(0, strlen($characters) - 1);
        $randomNumber .= $characters[$index];
      }
      $email = $userName . $randomNumber . '@test.com';
    }

    return $email;
  }

  /**
   * That is used to generate a token.
   *
   * @return string
   *   A token.
   *
   * @source from https://thisinterestsme.com/generating-random-token-php/
   */
  public function tokenGenerator() {
    do {
      // Generate a random string.
      $token = openssl_random_pseudo_bytes(40);

      // Convert the binary data into hexadecimal representation.
      $token = bin2hex($token);
    } while ($this->tokenChecker($token) > 0);

    return $token;
  }

  /**
   * Checked is token exists and return all information about the token.
   *
   * @param string $token
   *   User token.
   *
   * @return mixed
   *   An array.
   */
  public function tokenChecker($token) {
    $result = $this->connexion->select('vright', 'vr')
      ->fields('vr')
      ->condition('vr.token', $token, '=')
      ->countQuery();

    return $result->execute()->fetchField();
  }

  /**
   * Check if token is used.
   *
   * @param array $userRight
   *   User right information.
   *
   * @return bool
   *   Boolean.
   */
  public function isTokenUsed(array $userRight) {
    return $userRight['created'] !== $userRight['logged'];
  }

  /**
   * Generate random password.
   *
   * @return string
   *   Password as string.
   */
  public function randomPassword() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    // Remember to declare $pass as an array.
    $pass = [];
    // Put the length -1 in cache.
    $alphaLength = strlen($alphabet) - 1;
    for ($i = 0; $i < 8; $i++) {
      $index = rand(0, $alphaLength);
      $pass[] = $alphabet[$index];
    }
    return implode($pass);
  }

  /**
   * Generate user.
   *
   * @return bool|object|
   *   Return boolean or object.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   Entity storage exception.
   */
  public function userGenerator() {
    $email = $this->emailGenerator();
    $username = $this->nameGenerator();
    $password = $this->randomPassword();
    $user = User::create();

    // Mandatory settings.
    $user->setPassword($password);
    $user->enforceIsNew();
    $user->setEmail($email);
    $user->setUsername($username);
    $user->activate();

    // Save user.
    if ($user->save()) {
      return user_load_by_name($username);
    }
    return NULL;
  }

}
