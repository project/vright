<?php

namespace Drupal\vright\EventSubscriber;

use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Drupal\vright\VisitorRightManagerService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * When a user get access to the web site by free access link redirect the user.
 *
 * @package Drupal\vright\EventSubscriber
 */
class MaintenanceModeSubscriber implements EventSubscriberInterface {

  /**
   * The visitor right manager service.
   *
   * @var \Drupal\vright\VisitorRightManagerService
   */
  protected $visitorRightService;

  /**
   * MaintenanceModeSubscriber constructor.
   *
   * @param \Drupal\vright\VisitorRightManagerService $vrightService
   *   The visitor right manager service.
   */
  public function __construct(VisitorRightManagerService $vrightService) {
    $this->visitorRightService = $vrightService;
  }

  /**
   * Redirect the user according to right access.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $requestEvent
   *   The request event.
   */
  public function redirectVisitor(RequestEvent $requestEvent) {
    $request = $requestEvent->getRequest();
    $path = $request->attributes->get('_route');
    if (\Drupal::state()->get('system.maintenance_mode')) {
      // Check if user is anonymous and the path is the free access path.
      if (\Drupal::currentUser()->isAnonymous()) {
        if ($path === 'vright.access') {
          $token = \Drupal::routeMatch()->getParameter('token');
          // Get all information about the user right access.
          $userRight = $this->visitorRightService->getFreeAccess($token);
          // Authenticate user and redirect to homepage.
          if (count($userRight)) {
            if ($this->visitorRightService->hasFreeAccess($userRight) && !$this->visitorRightService->isTokenUsed($userRight)) {
              $user = User::load($userRight['uid']);
              user_login_finalize($user);
              // Set time of connexion.
              $userRight['logged'] = \Drupal::time()->getRequestTime();
              $this->visitorRightService->updateFreeAccess($userRight);
              return $requestEvent->setResponse(new RedirectResponse(Url::fromRoute('<front>')->toString()));
            }
          }
        }
      }

      // When user is authenticated.
      // Check access to the site in maintenance mode.
      $userRight = $this->visitorRightService->getVisitorRightByUserId(\Drupal::currentUser()->id());
      if (count($userRight) > 0) {
        if (!$this->visitorRightService->hasFreeAccess($userRight)) {
          user_logout();
        }
      }
    }

  }

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['redirectVisitor', 32];
    return $events;
  }

}
