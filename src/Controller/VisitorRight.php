<?php

namespace Drupal\vright\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class VisitorRight controller.
 *
 * @package Drupal\vright\Controller
 */
class VisitorRight extends ControllerBase {

  /**
   * Redirect to front.
   *
   * @param string $token
   *   The user token.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to front.
   */
  public function accessValidation($token) {
    if ($token != NULL) {
      return $this->redirect('<front>');
    }
    throw new AccessDeniedHttpException();
  }

}
