<?php

namespace Drupal\vright\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\vright\VisitorRightManagerService;
use Drupal\vright\VisitorRightManagerServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class VisitorRightManagerForm shows a table select to manage visitor.
 *
 * @package Drupal\vright\Form
 */
class VisitorRightManagerForm extends FormBase {

  /**
   * Visitor right service.
   *
   * @var \Drupal\vright\VisitorRightManagerService
   */
  protected $visitorRightService;

  /**
   * VisitorRightManagerForm constructor.
   *
   * @param \Drupal\vright\VisitorRightManagerService $rightService
   *   Visitor right service.
   */
  public function __construct(VisitorRightManagerService $rightService) {
    $this->visitorRightService = $rightService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('vright.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $visitors = $this->visitorRightService->getVisitors();
    $options = [];
    foreach ($visitors as $visitor) {
      $url = Url::fromRoute('vright.link', [
        'token' => $visitor['token'],
      ],
        [
          'attributes' => [
            'class' => [
              'use-ajax',
              'button',
            ],
            'data-dialog-type' => 'modal',
            'data-dialog-options' => Json::encode([
              'width' => 600,
            ]),
          ],
        ]);
      $options[$visitor['vid']] = [
        'name' => $visitor['name'],
        'created' => date('d/m/Y', $visitor['created']),
        'timing' => VisitorRightManagerServiceInterface::TIMING[$visitor['timing']],
        'url' => Link::fromTextAndUrl($this->t('Show free access link'), $url)->toString(),
        'status' => ucfirst($this->visitorRightService->getStatus($visitor)),
      ];
    }
    $form['generate'] = [
      '#type' => 'markup',
      '#markup' => Link::createFromRoute(
        'Generate new visitor',
        'vright.generator',
        [],
        [
          'attributes' => [
            'class' => [
              'use-ajax',
              'button button-action button--primary button--small',
            ],
            'data-dialog-type' => 'modal',
            'data-dialog-options' => Json::encode([
              'width' => 600,
            ]),
          ],
        ])->toString(),
    ];
    $form['break'] = [
      '#type' => 'markup',
      '#markup' => '<p></p>',
    ];
    $header = [
      'name' => $this->t('Username'),
      'created' => $this->t('Created at'),
      'timing' => $this->t('Timing'),
      'url' => $this->t('URL'),
      'status' => $this->t('Status'),
    ];
    $form['visitor'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => $this->t('No users found'),
    ];
    $form['btn'] = [
      '#type' => 'action',
    ];
    $form['btn']['delete'] = [
      '#type' => 'submit',
      '#value' => 'Delete access',
    ];
    $form['pager'] = [
      '#type' => 'pager',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vright_manager_form';
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $visitors = $form_state->getValue('visitor');
    if (count($visitors) > 0) {
      $matchedVisitor = array_filter($visitors, function ($visitor) {
        return ($visitor !== 0);
      });
      $visitorToRemove = array_values($matchedVisitor);
      $this->visitorRightService->removeFreeAccess($visitorToRemove);
      Drupal::messenger()->addStatus($this->t('The visitor selected has been removed'));
    }
  }

}
