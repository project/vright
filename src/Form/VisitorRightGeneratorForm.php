<?php

namespace Drupal\vright\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\vright\VisitorRightManagerService;
use Drupal\vright\VisitorRightManagerServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class VisitorRightGeneratorForm provides form to generate new visitor.
 *
 * @package Drupal\vright\Form
 */
class VisitorRightGeneratorForm extends FormBase {

  /**
   * Visitor right service.
   *
   * @var \Drupal\vright\VisitorRightManagerService
   */
  protected $visitorRightService;

  /**
   * VisitorRightGeneratorForm constructor.
   *
   * @param \Drupal\vright\VisitorRightManagerService $rightService
   *   The visitor right service.
   */
  public function __construct(VisitorRightManagerService $rightService) {
    $this->visitorRightService = $rightService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('vright.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $roles = $this->visitorRightService->getRoles();
    $timing = [
      0 => VisitorRightManagerServiceInterface::TIMING[0],
      15 => VisitorRightManagerServiceInterface::TIMING[15],
      30 => VisitorRightManagerServiceInterface::TIMING[30],
      60 => VisitorRightManagerServiceInterface::TIMING[60],
      120 => VisitorRightManagerServiceInterface::TIMING[120],
    ];

    $form['roles'] = [
      '#type' => 'checkboxes',
      '#multiple' => TRUE,
      '#options' => $roles,
      '#default_value' => VisitorRightManagerServiceInterface::VR_VISITOR,
      '#title' => $this->t('Roles'),
    ];
    $form['timing'] = [
      '#type' => 'select',
      '#options' => $timing,
      '#defaul_value' => 0,
      '#title' => $this->t('Timing'),
    ];

    $form['btn'] = [
      '#type' => 'action',
    ];
    $form['btn']['generate'] = [
      '#type' => 'submit',
      '#value' => 'Generate',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vright_generator_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $user = $this->visitorRightService->userGenerator();
    $roles = $form_state->getValue('roles');
    $timing = $form_state->getValue('timing');
    $matchedRoles = array_filter($roles, function ($role) {
      return ($role !== 0);
    });

    // Set vr_visitor as default role.
    $user->addRole(VisitorRightManagerServiceInterface::VR_VISITOR);
    foreach ($matchedRoles as $matchedRole) {
      $user->addRole($matchedRole);
    }
    $user->save();

    $token = $this->visitorRightService->tokenGenerator();
    $this->visitorRightService->createFreeAccess($user, $token, $timing);
    \Drupal::messenger()->addStatus($this->t('Free access has been created for user @username', ['@username' => $user->getAccountName()]));
    $form_state->setRedirect('vright.manager');
  }

}
