<?php

namespace Drupal\vright\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\vright\VisitorRightManagerService;
use Drupal\vright\VisitorRightManagerServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class VisitorRightLinkFor shows a form that contain visitor free access link.
 *
 * @package Drupal\vright\Form
 */
class VisitorRightLinkForm extends FormBase {

  /**
   * The visitor right manager service.
   *
   * @var \Drupal\vright\VisitorRightManagerService
   */
  protected $visitorRightService;

  /**
   * VisitorRightLinkForm constructor.
   *
   * @param \Drupal\vright\VisitorRightManagerService $rightService
   *   The right service.
   */
  public function __construct(VisitorRightManagerService $rightService) {
    $this->visitorRightService = $rightService;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vright_link_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $token = \Drupal::routeMatch()->getParameter('token');
    $userRight = $this->visitorRightService->getVisitorRightByToken($token);
    $basePath = $this->getRequest()->getSchemeAndHttpHost();
    $url = $basePath . Url::fromRoute('vright.access', ['token' => $token])->toString();
    $text = $this->t('You have an access to the website @site_name for @time. Copy the following url into your web browser to access to the platform @url',
      [
        '@site_name' => \Drupal::config('system.site')->get('name'),
        '@time' => VisitorRightManagerServiceInterface::TIMING[$userRight['timing']],
        '@url' => $url,
      ]);
    $form['textarea'] = [
      '#type' => 'textarea',
      '#value' => $text,
      '#title' => $this->t('Copy the text bellow.'),
      '#attributes' => [
        'col' => '120',
        'row' => '10',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('vright.manager')
    );
  }

}
