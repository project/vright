<?php

namespace Drupal\vright;

use Drupal\Core\Database\Connection;
use Drupal\user\Entity\User;
use Drupal\user\RoleInterface;
use Drupal\vright\Traits\GeneratorTrait;

/**
 * Class VisitorRightManagerService manage the visitor access right.
 *
 * @package Drupal\vright
 */
class VisitorRightManagerService implements VisitorRightManagerServiceInterface {

  use GeneratorTrait;

  /**
   * Database connexion.
   *
   * @var Database
   */
  protected $connexion;

  /**
   * VisitorRightManagerService constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   Database connexion.
   */
  public function __construct(Connection $database) {
    $this->connexion = $database;
  }

  /**
   * {@inheritDoc}
   */
  public function getRoles() {
    $roles = [];
    $userRoles = user_roles(TRUE);
    foreach ($userRoles as $userRole) {
      if (!in_array($userRole->id(), [
        RoleInterface::AUTHENTICATED_ID,
        self::VR_VISITOR,
      ])) {
        $roles[$userRole->id()] = $userRole->label();
      }
    }

    return $roles;
  }

  /**
   * Get the visitor right information by token.
   *
   * @param string $token
   *   The visitor token.
   *
   * @return \Drupal\Core\Database\StatementInterface|mixed|null
   *   \Array
   *   \Null.
   */
  public function getVisitorRightByToken($token) {
    $result = $this->connexion->select('vright', 'vr')
      ->fields('vr')
      ->condition('vr.token', $token, '=')
      ->range(0, 1)
      ->execute();
    $userDatas = $result->fetchAll();
    $user = [];
    foreach ($userDatas as $userData) {
      $user = (array) $userData;
    }
    return $user;
  }

  /**
   * {@inheritDoc}
   */
  public function getVisitorRightByUserId($userId) {
    $result = $this->connexion->select('vright', 'vr')
      ->fields('vr')
      ->condition('vr.uid', $userId, '=')
      ->range(0, 1)
      ->execute();
    $userDatas = $result->fetchAll();
    $user = [];
    foreach ($userDatas as $userData) {
      $user = (array) $userData;
    }
    return $user;
  }

  /**
   * {@inheritDoc}
   */
  public function createFreeAccess(User $user, $token, $timing) {
    $requestTime = \Drupal::time()->getRequestTime();
    return $this->connexion->insert('vright')
      ->fields([
        'uid' => $user->id(),
        'token' => $token,
        'created' => $requestTime,
        'timing' => $timing,
        'logged' => $requestTime,
      ])
      ->execute();
  }

  /**
   * {@inheritDoc}
   */
  public function updateFreeAccess($userRight) {
    return $this->connexion->update('vright')
      ->fields([
        'uid' => $userRight['uid'],
        'token' => $userRight['token'],
        'created' => $userRight['created'],
        'timing' => $userRight['timing'],
        'logged' => $userRight['logged'],
      ])
      ->condition('vid', $userRight['vid'], '=')
      ->execute();
  }

  /**
   * {@inheritDoc}
   */
  public function hasFreeAccess($userRight) {
    if ($this->getStatus($userRight) !== 'expired') {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get the status of the user.
   *
   * @param array $userRight
   *   The user right information.
   *
   * @return string
   *   return the status as string
   */
  public function getStatus(array $userRight) {
    // If user hasn't logged yet.
    if (!$this->isTokenUsed($userRight)) {
      return 'active';
    }

    // If user has unlimited access time.
    if ($userRight['timing'] == 0) {
      return 'used';
    }

    $currentTime = \Drupal::time()->getRequestTime();
    $expirationTime = strtotime('+' . $userRight['timing'] . ' minutes', $userRight['logged']);
    if ($currentTime < $expirationTime) {
      return 'used';
    }
    return 'expired';
  }

  /**
   * {@inheritDoc}
   */
  public function removeFreeAccess($visitorIds) {
    return $this->connexion->delete('vright')
      ->condition('vid', $visitorIds, 'IN')
      ->execute();
  }

  /**
   * {@inheritDoc}
   */
  public function userRightDatas($token) {
    return $this->getVisitorRightByToken($token);
  }

  /**
   * {@inheritDoc}
   */
  public function getFreeAccess($token) {
    $userRight = $this->userRightDatas($token);
    if ($userRight) {
      return $userRight;
    }
    return NULL;
  }

  /**
   * Get all information about the visitor.
   *
   * @return array
   *   return visitor as array
   */
  public function getVisitors() {
    $query = $this->connexion->select('vright', 'vr')
      ->fields('vr');
    $query->leftJoin('users_field_data', 'u', 'vr.uid = u.uid');
    $query->addField('u', 'name');
    $query->orderBy('vr.vid', 'DESC');
    $pager = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(10);
    $results = $pager->execute()->fetchAll();
    $visitors = [];
    foreach ($results as $result) {
      $visitors[] = (array) $result;
    }
    return $visitors;
  }

}
