<?php

namespace Drupal\vright;

use Drupal\user\Entity\User;

/**
 * Interface VisitorRightManagerServiceInterface manages the visitor access.
 *
 * @package Drupal\vright
 */
interface VisitorRightManagerServiceInterface {

  const VR_VISITOR = 'vr_visitor';

  const TIMING = [
    0 => 'Unlimited',
    15 => '15 minutes',
    30 => '30 minutes',
    60 => '60 minutes',
    120 => '120 minutes',
  ];

  /**
   * Get the all roles.
   *
   * @return mixed
   *   \Array of data
   */
  public function getRoles();

  /**
   * Get visitor by the token.
   *
   * @param string $token
   *   The user token.
   *
   * @return \Drupal\Core\Database\StatementInterface|mixed|null
   *   \Array of data
   *   \Null
   */
  public function getVisitorRightByToken($token);

  /**
   * Get the visitor by user Id.
   *
   * @param int $userId
   *   The user ID.
   *
   * @return \Drupal\Core\Database\StatementInterface|mixed|null
   *   \Array of data
   *   \Null
   */
  public function getVisitorRightByUserId($userId);

  /**
   * Create free access for visitor.
   *
   * @param \Drupal\user\Entity\User $user
   *   The user.
   * @param string $token
   *   The user token.
   * @param int $timing
   *   The token duration.
   *
   * @return int
   *   Integer as the state of the request.
   */
  public function createFreeAccess(User $user, $token, $timing);

  /**
   * Return the free access of visitor it that exists.
   *
   * @param string $token
   *   The user token.
   *
   * @return mixed
   *   Data array.
   */
  public function getFreeAccess($token);

  /**
   * All information about visitor right.
   *
   * @param string $token
   *   The user token.
   *
   * @return mixed
   *   Data array.
   */
  public function userRightDatas($token);

  /**
   * Remove the free access.
   *
   * @param array $userRight
   *   The user right information.
   *
   * @return mixed
   *   Data array.
   */
  public function removeFreeAccess(array $userRight);

  /**
   * Check if visitor has free access.
   *
   * @param array $userRight
   *   The user right information.
   *
   * @return bool
   *   Boolean.
   */
  public function hasFreeAccess(array $userRight);

  /**
   * Update visitor free access.
   *
   * @param array $userRight
   *   The user right information.
   *
   * @return mixed
   *   Data array.
   */
  public function updateFreeAccess(array $userRight);

}
